package com.adyen.android.assignment.presentation.theme

import androidx.compose.ui.graphics.Color


val PrimaryColor = Color(0xFF4CAF50)
val PrimaryLightColor = Color(0xFFC8E6C9)
val PrimaryDarkColor = Color(0xFF388E3C)
val AccentColor = Color(0x8BC34A)
val PrimaryTextColor = Color(0x212121)
val SecondaryTextColor = Color(0x757575)
val TextIconColor = Color(0xFFFFFF)
val DividerColor = Color(0xBDBDBD)


