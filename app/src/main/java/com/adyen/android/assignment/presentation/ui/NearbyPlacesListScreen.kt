package com.adyen.android.assignment.presentation.ui

import android.Manifest
import android.content.Intent
import android.provider.Settings.ACTION_SETTINGS
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Place
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.adyen.android.assignment.R
import com.adyen.android.assignment.data.api.model.Category
import com.adyen.android.assignment.data.api.model.PlaceResult
import com.adyen.android.assignment.presentation.theme.Dimension
import com.adyen.android.assignment.presentation.viewmodel.NearbyPlacesListViewModel
import com.adyen.android.assignment.presentation.viewmodel.NearbyPlacesListViewModel.UiAction
import com.adyen.android.assignment.presentation.viewmodel.NearbyPlacesListViewModel.UiEvent
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun NearbyPlacesListScreen(
    viewModel: NearbyPlacesListViewModel = hiltViewModel()
) {

    val context = LocalContext.current
    val permissionState = rememberPermissionState(Manifest.permission.ACCESS_COARSE_LOCATION)

    val lifecycleOwner = LocalLifecycleOwner.current
    DisposableEffect(
        key1 = lifecycleOwner,
        effect = {
            val observer = LifecycleEventObserver { _, event ->
                if (event == Lifecycle.Event.ON_START) {
                    permissionState.launchPermissionRequest()
                }
            }
            lifecycleOwner.lifecycle.addObserver(observer)

            onDispose {
                lifecycleOwner.lifecycle.removeObserver(observer)
            }
        }
    )

    val uiEventState = viewModel.uiEventState.collectAsState(initial = UiEvent.OnLoading)

    with(uiEventState.value) {
        when (this) {
            is UiEvent.OnLoading ->
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center,
                ) {
                    CircularProgressIndicator()
                }
            is UiEvent.OnDisplayData -> NearbyPlacesListWidget(this.results)
            is UiEvent.OnError -> ErrorWidget { viewModel.perform(UiAction.OnFetch) }
            is UiEvent.OnPermissionRequired -> LocationPermissionWidget(
                permissionState,
                onPermissionGranted = {
                    viewModel.perform(UiAction.OnFetch)
                },
                onNavigateToSettings = {
                    context.startActivity(Intent(ACTION_SETTINGS))
                })
        }
    }
}

@Composable
fun NearbyPlacesListWidget(
    places: List<PlaceResult>
) {
    LazyColumn {
        items(
            items = places,
            itemContent = {
                NearbyPlaceRowWidget(
                    name = it.name,
                    categories = it.categories
                )
                Divider(
                    color = Color.LightGray,
                    thickness = 0.5f.dp
                )
            }
        )
    }
}

@Composable
fun NearbyPlaceRowWidget(
    name: String,
    categories: List<Category>
) {

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(Dimension.small)
    ) {
        Box(
            modifier = Modifier
                .size(Dimension.xxxLarge)
                .clip(CircleShape)
                .background(MaterialTheme.colors.primary),
            contentAlignment = Alignment.Center
        ) {
            Icon(
                Icons.Outlined.Place,
                modifier = Modifier
                    .size(Dimension.large),
                tint = Color.White,
                contentDescription = stringResource(R.string.localized_description)
            )
        }
        Spacer(modifier = Modifier.size(Dimension.small))
        Column(
            modifier = Modifier
                .fillMaxWidth()

        ) {
            Text(
                modifier = Modifier
                    .padding(horizontal = Dimension.small),
                text = name,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold
            )
            Row {
                categories.take(3).forEach {
                    Text(
                        text = it.name,
                        maxLines = 1,
                        fontSize = 12.sp,
                        modifier = Modifier
                            .padding(Dimension.xsmall)
                            .clip(CutCornerShape(Dimension.xxsmall))
                            .background(Color.LightGray)
                            .padding(horizontal = Dimension.medium, vertical = Dimension.xsmall)
                    )
                }
            }
        }

    }
}
