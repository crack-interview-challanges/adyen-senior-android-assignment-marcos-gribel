package com.adyen.android.assignment.core.di

import com.adyen.android.assignment.data.datasource.PlacesService
import com.adyen.android.assignment.domain.repository.PlacesRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepoModule {

    @Singleton
    @Provides
    fun providePlacesRepository(
        service: PlacesService
    ): PlacesRepo = PlacesRepo(
        service = service
    )
}