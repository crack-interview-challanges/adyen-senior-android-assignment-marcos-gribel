package com.adyen.android.assignment.presentation.ui

import android.Manifest
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Place
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.adyen.android.assignment.R
import com.adyen.android.assignment.presentation.theme.Dimension
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.shouldShowRationale

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun LocationPermissionWidget(
    permissionState: PermissionState,
    onPermissionGranted: () -> Unit,
    onNavigateToSettings: () -> Unit,
) {
    Column(
        modifier = Modifier
            .padding(Dimension.medium)
            .fillMaxSize(),
    ) {
        Column(
            modifier = Modifier.weight(2F),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Icon(
                Icons.Default.Place,
                modifier = Modifier
                    .size(Dimension.logo)
                    .padding(vertical = Dimension.medium),
                contentDescription = stringResource(R.string.localized_description)
            )
            Text(
                modifier = Modifier.padding(vertical = Dimension.medium),
                text = stringResource(R.string.title_location_permission),
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp
            )
            Text(
                modifier = Modifier.padding(vertical = Dimension.medium),
                text = stringResource(R.string.body_location_permission),
                textAlign = TextAlign.Center,
            )
        }

        when (permissionState.permission) {
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION -> {
                when {
                    permissionState.status.isGranted -> {
                        onPermissionGranted()
                    }
                    permissionState.status.shouldShowRationale -> {
                        PrimaryButtonWidget(onClick = {
                            permissionState.launchPermissionRequest()
                        }) {
                            Text(
                                text = stringResource(R.string.btn_enable_location),
                                color = Color.White
                            )
                        }
                    }
                    /* Permanently Denied */
                    permissionState.status.isGranted.not() && permissionState.status.shouldShowRationale.not() -> {
                        PrimaryButtonWidget(onClick = onNavigateToSettings) {
                            Text(text = stringResource(R.string.btn_settings),
                                color = Color.White
                            )
                        }
                    }
                }
            }
        }

    }
}