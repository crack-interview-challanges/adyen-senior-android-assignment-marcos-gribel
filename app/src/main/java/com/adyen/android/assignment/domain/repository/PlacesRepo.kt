package com.adyen.android.assignment.domain.repository

import com.adyen.android.assignment.data.api.VenueRecommendationsQueryBuilder
import com.adyen.android.assignment.data.api.model.ResponseWrapper
import com.adyen.android.assignment.data.datasource.PlacesService
import com.adyen.android.assignment.data.entities.Result
import javax.inject.Inject

class PlacesRepo @Inject constructor(
    private val service: PlacesService
) {

    suspend fun getVenueRecommendations(
        lat: Double,
        lon: Double,
    ): Result<ResponseWrapper?> = VenueRecommendationsQueryBuilder()
        .setLatitudeLongitude(latitude = lat, longitude = lon)
        .build()
        .let {
            service.getVenueRecommendations(it)
        }

}