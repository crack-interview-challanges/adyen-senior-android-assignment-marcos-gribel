package com.adyen.android.assignment.presentation.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adyen.android.assignment.data.api.model.PlaceResult
import com.adyen.android.assignment.data.entities.Result
import com.adyen.android.assignment.domain.usecase.GetNearbyPlacesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NearbyPlacesListViewModel @Inject constructor(
    private val getNearbyPlacesUseCase: GetNearbyPlacesUseCase,
) : ViewModel() {

    sealed class UiAction {
        object OnFetch : UiAction()
    }

    sealed class UiEvent {
        object OnLoading : UiEvent()
        data class OnDisplayData(val results: List<PlaceResult>) : UiEvent()
        data class OnError(val message: String?) : UiEvent()
        object OnPermissionRequired : UiEvent()
    }

    private val _uiEventState = MutableStateFlow<UiEvent>(UiEvent.OnLoading)
    val uiEventState: Flow<UiEvent> = _uiEventState

    init {
        perform(UiAction.OnFetch)
    }

    fun perform(action: UiAction) {
        when (action) {
            UiAction.OnFetch -> fetch()
        }
    }

    @SuppressLint("MissingPermission")
    private fun fetch() {
        viewModelScope.launch {
            getNearbyPlacesUseCase.invoke()
                .onStart {
                    _uiEventState.value = UiEvent.OnLoading
                }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            _uiEventState.value = UiEvent.OnDisplayData(
                                result.data?.results ?: emptyList()
                            )
                        }
                        is Result.Error -> {
                            _uiEventState.value = when (result.exception) {
                                is GetNearbyPlacesUseCase.PermissionRequiredException -> UiEvent.OnPermissionRequired
                                else -> UiEvent.OnError(result.exception.message)
                            }
                        }
                    }
                }
        }
    }

}