package com.adyen.android.assignment.core.di

import com.adyen.android.assignment.BuildConfig
import com.adyen.android.assignment.core.RetrofitConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {


    @Singleton
    @Provides
    fun provideNetworkConfig(): Retrofit = RetrofitConfig(
        baseUrl = BuildConfig.FOURSQUARE_BASE_URL,
    ).invoke()

}