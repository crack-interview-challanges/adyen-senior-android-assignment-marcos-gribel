package com.adyen.android.assignment.data.datasource

import com.adyen.android.assignment.data.entities.Result
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class ServiceManager @Inject constructor(
    private val retrofit: Retrofit
) {

    suspend fun <T> execute(
        apiCall: suspend () -> Response<T>,
        defaultErrorMessage: String = "Something went wrong!"
    ): Result<T> {
        return try {
            with(apiCall()) {
                if (isSuccessful) {
                    Result.Success(this.body()!!)
                } else {
                    val converter = retrofit.responseBodyConverter<Error>(
                        Error::class.java,
                        arrayOfNulls(0)
                    )
                    val error = converter.convert(errorBody()!!)
                    Result.Error(error ?: Throwable(defaultErrorMessage))
                }
            }
        } catch (e: Throwable) {
            Result.Error(e)
        }
    }
}