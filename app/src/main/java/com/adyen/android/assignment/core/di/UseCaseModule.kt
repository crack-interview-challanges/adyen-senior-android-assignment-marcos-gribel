package com.adyen.android.assignment.core.di

import android.content.Context
import com.adyen.android.assignment.domain.repository.PlacesRepo
import com.adyen.android.assignment.domain.usecase.GetNearbyPlacesUseCase
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Singleton
    @Provides
    fun provideFusedLocationProviderClient(
        @ApplicationContext context: Context
    ): FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

    @Singleton
    @Provides
    fun provideGetNearbyPlacesUseCase(
        repository: PlacesRepo,
        fusedLocationProvider: FusedLocationProviderClient
    ) = GetNearbyPlacesUseCase(
        repository = repository,
        fusedLocationProvider = fusedLocationProvider,
        coroutineScope = Dispatchers.IO
    )

}