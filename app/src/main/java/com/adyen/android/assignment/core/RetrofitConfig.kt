package com.adyen.android.assignment.core

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject

class RetrofitConfig @Inject constructor(
    private val baseUrl: String,
) {

    operator fun invoke(): Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

}