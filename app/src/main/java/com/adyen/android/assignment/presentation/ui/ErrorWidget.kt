package com.adyen.android.assignment.presentation.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.adyen.android.assignment.R
import com.adyen.android.assignment.presentation.theme.Dimension

@Composable
fun ErrorWidget(
    onTryAgainClick: () -> Unit
) {
    Column(
        modifier = Modifier
            .padding(Dimension.medium)
            .fillMaxSize(),
    ) {
        Column(
            modifier = Modifier.weight(2F),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Icon(
                Icons.Outlined.Close,
                modifier = Modifier
                    .size(Dimension.logo)
                    .padding(vertical = Dimension.medium),
                contentDescription = stringResource(R.string.localized_description)
            )
        Text(
            modifier = Modifier.padding(vertical = Dimension.medium),
            text = stringResource(R.string.body_something_went_wrong),
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Normal,
            fontSize = 24.sp
        )
        }
        PrimaryButtonWidget(onClick = onTryAgainClick) {
            Text(text = stringResource(R.string.btn_try_again),
                color = Color.White)
        }
    }
}
