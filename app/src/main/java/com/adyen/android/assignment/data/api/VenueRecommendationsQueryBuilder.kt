package com.adyen.android.assignment.data.api

class VenueRecommendationsQueryBuilder : PlacesQueryBuilder() {
    private var latitudeLongitude: String? = null
    private var limit: Int = 50

    fun setLatitudeLongitude(
        latitude: Double,
        longitude: Double
    ): VenueRecommendationsQueryBuilder {
        this.latitudeLongitude = "$latitude,$longitude"
        return this
    }

    /* The number of results to return, min 1 up to 50. Defaults to 10. */
    fun setLimit(limit: Int): VenueRecommendationsQueryBuilder {
        this.limit = limit
        return this
    }

    override fun putQueryParams(queryParams: MutableMap<String, String>) {
        latitudeLongitude?.apply { queryParams["ll"] = this }
        limit.apply { queryParams["limit"] = this.toString() }
    }
}
