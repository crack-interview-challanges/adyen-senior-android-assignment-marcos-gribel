package com.adyen.android.assignment.presentation.theme

import androidx.compose.ui.unit.dp

object Dimension {
    val xxsmall = 2.dp
    val xsmall = 4.dp
    val small = 8.dp
    val medium = 16.dp
    val large = 24.dp
    val xLarge = 32.dp
    val xxLarge = 48.dp
    val xxxLarge = 56.dp

    val logo = 150.dp
}