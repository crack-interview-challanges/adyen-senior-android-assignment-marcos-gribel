package com.adyen.android.assignment.core.di

import com.adyen.android.assignment.data.datasource.PlacesEndpoint
import com.adyen.android.assignment.data.datasource.PlacesService
import com.adyen.android.assignment.data.datasource.ServiceManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ServiceModule {

    @Singleton
    @Provides
    fun providePlacesEndpoint(retrofit: Retrofit): PlacesEndpoint =
        retrofit.create(PlacesEndpoint::class.java)

    @Singleton
    @Provides
    fun provideServiceManager(
        retrofit: Retrofit
    ) = ServiceManager(
        retrofit = retrofit
    )

    @Singleton
    @Provides
    fun providePlacesService(
        serviceManager: ServiceManager,
        endpoint: PlacesEndpoint
    ) = PlacesService(
        serviceManager = serviceManager,
        endpoint = endpoint
    )

}