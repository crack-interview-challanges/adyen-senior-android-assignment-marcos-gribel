package com.adyen.android.assignment.presentation.ui

import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Button
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.adyen.android.assignment.presentation.theme.Dimension

@Composable
fun PrimaryButtonWidget(
    onClick: () -> Unit,
    content: @Composable RowScope.() -> Unit
) {
    Button(
        onClick = onClick,
        modifier = Modifier
            .fillMaxWidth()
            .size(Dimension.xxxLarge)
            .padding(horizontal = Dimension.medium),
        content = content
    )
}
