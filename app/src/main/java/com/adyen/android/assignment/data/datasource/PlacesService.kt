package com.adyen.android.assignment.data.datasource

import com.adyen.android.assignment.BuildConfig
import com.adyen.android.assignment.data.api.model.ResponseWrapper
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.QueryMap
import javax.inject.Inject

interface PlacesEndpoint {

    companion object {
        const val LATITUDE_DEFAULT = 52.376510
        const val LONGITUDE_DEFAULT = 4.905890
    }
    /**
     * Get venue recommendations.
     *
     * See [the docs](https://developer.foursquare.com/reference/places-nearby)
     */
    @Headers("Authorization: ${BuildConfig.API_KEY}")
    @GET("places/nearby")
    suspend fun getVenueRecommendations(@QueryMap query: Map<String, String>): Response<ResponseWrapper>
}

class PlacesService @Inject constructor(
    private val serviceManager: ServiceManager,
    private val endpoint: PlacesEndpoint
) {

    suspend fun getVenueRecommendations(
        @QueryMap query: Map<String, String>
    ): com.adyen.android.assignment.data.entities.Result<ResponseWrapper?> =
        serviceManager.execute({
            endpoint.getVenueRecommendations(query)
        })

}


