package com.adyen.android.assignment.domain.usecase


import android.Manifest.permission.ACCESS_COARSE_LOCATION
import androidx.annotation.RequiresPermission
import com.adyen.android.assignment.data.api.model.ResponseWrapper
import com.adyen.android.assignment.data.datasource.PlacesEndpoint
import com.adyen.android.assignment.data.datasource.PlacesEndpoint.Companion.LONGITUDE_DEFAULT
import com.adyen.android.assignment.data.entities.Result
import com.adyen.android.assignment.domain.repository.PlacesRepo
import com.google.android.gms.location.FusedLocationProviderClient
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class GetNearbyPlacesUseCase @Inject constructor(
    private val repository: PlacesRepo,
    private val fusedLocationProvider: FusedLocationProviderClient,
    private val coroutineScope: CoroutineDispatcher,
) {

    @RequiresPermission(anyOf = [ACCESS_COARSE_LOCATION])
    suspend operator fun invoke(): Flow<Result<ResponseWrapper?>> = flow<Result<ResponseWrapper?>> {
        try {
            val location = fusedLocationProvider.lastLocation.await()
            val result = if (location == null) {
                repository.getVenueRecommendations(PlacesEndpoint.LATITUDE_DEFAULT, LONGITUDE_DEFAULT)
            } else {
                repository.getVenueRecommendations(location.latitude, location.longitude)
            }
            emit(result)
        } catch (e: SecurityException) {
            emit(
                Result.Error(
                    PermissionRequiredException(
                        message = e.message ?: "Permission Required",
                        permissions = arrayOf(ACCESS_COARSE_LOCATION),
                        cause = e
                    )
                )
            )
        } catch (e: java.lang.Exception) {
            emit(Result.Error(e))
        }
    }.flowOn(coroutineScope)

    class PermissionRequiredException(
        message: String,
        permissions: Array<String>,
        cause: Throwable? = null
    ) : Exception(message, cause)
}