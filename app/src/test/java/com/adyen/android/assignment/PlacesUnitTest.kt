package com.adyen.android.assignment

import com.adyen.android.assignment.core.RetrofitConfig
import com.adyen.android.assignment.data.api.VenueRecommendationsQueryBuilder
import com.adyen.android.assignment.data.datasource.PlacesEndpoint
import com.adyen.android.assignment.data.datasource.PlacesEndpoint.Companion.LATITUDE_DEFAULT
import com.adyen.android.assignment.data.datasource.PlacesEndpoint.Companion.LONGITUDE_DEFAULT
import com.adyen.android.assignment.data.datasource.PlacesService
import com.adyen.android.assignment.data.datasource.ServiceManager
import com.adyen.android.assignment.data.entities.Result
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Test

class PlacesUnitTest {

    @Test
    fun testResponseCode() = runBlocking {
        val retrofit = RetrofitConfig(
            baseUrl = BuildConfig.FOURSQUARE_BASE_URL
        ).invoke()
        val serviceManager = ServiceManager(retrofit)
        val service = PlacesService(
            serviceManager,
            retrofit.create(PlacesEndpoint::class.java),
        )

        val query = VenueRecommendationsQueryBuilder()
            .setLatitudeLongitude(LATITUDE_DEFAULT, LONGITUDE_DEFAULT)
            .build()
        val response = service.getVenueRecommendations(query)
        val responseWrapper = response
        assertTrue(response is Result.Success)
        assertNotNull("Response is null.", responseWrapper)
    }

}
