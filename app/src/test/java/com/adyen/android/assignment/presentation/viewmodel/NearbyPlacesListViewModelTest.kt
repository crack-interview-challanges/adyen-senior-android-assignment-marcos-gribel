package com.adyen.android.assignment.presentation.viewmodel

import app.cash.turbine.test
import com.adyen.android.assignment.data.api.model.ResponseWrapper
import com.adyen.android.assignment.data.entities.Result
import com.adyen.android.assignment.domain.usecase.GetNearbyPlacesUseCase
import com.adyen.android.assignment.domain.usecase.GetNearbyPlacesUseCase.PermissionRequiredException
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class NearbyPlacesListViewModelTest {

    private val testDispatcher = TestCoroutineDispatcher()

    private val useCaseMock = mockk<GetNearbyPlacesUseCase>()

    @Before
    fun setUop() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when create ViewModel then fetch data`() = runTest {
        //Given
        val response = ResponseWrapper(emptyList())
        coEvery { useCaseMock.invoke() } returns flowOf(Result.Success(response))

        // When
        val viewModel = NearbyPlacesListViewModel(useCaseMock)

        // Then
        viewModel.uiEventState.test {
            Assert.assertEquals(
                NearbyPlacesListViewModel.UiEvent.OnDisplayData(emptyList()),
                awaitItem()
            )
        }
        coVerify(exactly = 1) { useCaseMock.invoke() }
    }

    @Test
    fun `when fetch failed due to permission is not granted then emit OnPermissionRequired event`() =
        runTest {
            //Given
            coEvery { useCaseMock.invoke() } returns flowOf(Result.Error(mockk<PermissionRequiredException>()))

            // When
            val viewModel = NearbyPlacesListViewModel(useCaseMock)

            // Then
            viewModel.uiEventState.test {
                Assert.assertEquals(
                    NearbyPlacesListViewModel.UiEvent.OnPermissionRequired,
                    awaitItem()
                )
            }
            coVerify(exactly = 1) { useCaseMock.invoke() }
        }


    @Test
    fun `when fetch failed due to any reason then emit OnError event`() = runTest {
        //Given
        val msgMock = "Error Mock Message"
        val exception = mockk<Exception>(msgMock)
        every { exception.message } returns  msgMock
        coEvery { useCaseMock.invoke() } returns flowOf(Result.Error(exception))

        // When
        val viewModel = NearbyPlacesListViewModel(useCaseMock)

        // Then
        viewModel.uiEventState.test {
            Assert.assertEquals(
                NearbyPlacesListViewModel.UiEvent.OnError(msgMock),
                awaitItem()
            )
        }

        coVerify(exactly = 1) { useCaseMock.invoke() }
    }
}
