package com.adyen.android.assignment.domain.usecase

import android.location.Location
import com.adyen.android.assignment.data.api.model.ResponseWrapper
import com.adyen.android.assignment.data.datasource.PlacesEndpoint
import com.adyen.android.assignment.data.entities.Result
import com.adyen.android.assignment.domain.repository.PlacesRepo
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.tasks.Task
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import java.io.IOException

@OptIn(ExperimentalCoroutinesApi::class)
class GetNearbyPlacesUseCaseTest {

    private val repoMock = mockk<PlacesRepo>()
    private val fusedLocationMock = mockk<FusedLocationProviderClient>()
    private val dispatcher = Dispatchers.Default

    private val taskLocationMock = mockk<Task<Location>>()

    @Before
    fun setUp() {
        every { taskLocationMock.isSuccessful } returns true
        every { taskLocationMock.isComplete } returns true
        every { taskLocationMock.isCanceled } returns false
        every { taskLocationMock.exception } returns null
    }


    @Test
    fun `when location not found then fetch with defaults lat and long`() = runTest {
        //Given
        every { taskLocationMock.result } returns null
        every { fusedLocationMock.lastLocation } returns taskLocationMock
        coEvery {
            repoMock.getVenueRecommendations(any(), any())
        } returns Result.Success(ResponseWrapper(emptyList()))

        // When
        val useCase = GetNearbyPlacesUseCase(
            repoMock,
            fusedLocationMock,
            dispatcher
        )
        useCase.invoke().collect()

        //Then
        coVerify {
            repoMock.getVenueRecommendations(
                PlacesEndpoint.LATITUDE_DEFAULT,
                PlacesEndpoint.LONGITUDE_DEFAULT
            )
        }
    }


    @Test
    fun `when location  found then fetch with lat and long`() = runTest {
        //Given
        val latitude = 4.0
        val longitude = 5.0

        val locationMock = mockk<Location>()
        every { locationMock.latitude } returns latitude
        every { locationMock.longitude } returns longitude
        every { taskLocationMock.result } returns locationMock
        every { fusedLocationMock.lastLocation } returns taskLocationMock

        coEvery {
            repoMock.getVenueRecommendations(latitude, longitude)
        } returns Result.Success(ResponseWrapper(emptyList()))

        // When
        val useCase = GetNearbyPlacesUseCase(
            repoMock,
            fusedLocationMock,
            dispatcher
        )
        useCase.invoke().collect()

        //Then
        coVerify {
            repoMock.getVenueRecommendations(
                latitude,
                longitude
            )
        }
    }

    @Test
    fun `when location permission is not granted then emit PermissionRequiredException`() = runTest {
        //Given
        every { taskLocationMock.isSuccessful } returns false
        every { taskLocationMock.exception } returns SecurityException()
        every { fusedLocationMock.lastLocation } returns taskLocationMock

        // When
        val useCase = GetNearbyPlacesUseCase(
            repoMock,
            fusedLocationMock,
            dispatcher
        )

        //Then
        useCase.invoke().collect {
            assert(it is Result.Error)
            assert((it as Result.Error).exception is GetNearbyPlacesUseCase.PermissionRequiredException)
        }
    }

    @Test
    fun `when an exception occur then emit an general result error with its exception`() = runTest {
        //Given
        every { taskLocationMock.isSuccessful } returns false
        every { taskLocationMock.exception } returns IOException()
        every { fusedLocationMock.lastLocation } returns taskLocationMock

        // When
        val useCase = GetNearbyPlacesUseCase(
            repoMock,
            fusedLocationMock,
            dispatcher
        )

        //Then
        useCase.invoke().collect {
            assert(it is Result.Error)
            assert((it as Result.Error).exception is IOException)
        }
    }

}