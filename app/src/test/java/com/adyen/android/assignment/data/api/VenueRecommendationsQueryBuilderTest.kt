package com.adyen.android.assignment.data.api

import com.adyen.android.assignment.data.datasource.PlacesEndpoint.Companion.LATITUDE_DEFAULT
import com.adyen.android.assignment.data.datasource.PlacesEndpoint.Companion.LONGITUDE_DEFAULT
import junit.framework.TestCase.assertEquals
import org.junit.Test

class VenueRecommendationsQueryBuilderTest {

    @Test
    fun `when limit is not set then defaults to 50`() {
        val builder = VenueRecommendationsQueryBuilder().build()
        assertEquals("50", builder["limit"])
    }

    @Test
    fun `when limit is set then update the query params`() {
        val builder = VenueRecommendationsQueryBuilder()
            .setLimit(5)
            .build()
        assertEquals("5", builder["limit"])
    }

    @Test
    fun `when set lat and long then update query params`() {

        val builder = VenueRecommendationsQueryBuilder()
            .setLatitudeLongitude(LATITUDE_DEFAULT, LONGITUDE_DEFAULT)
            .build()
        assertEquals("$LATITUDE_DEFAULT,$LONGITUDE_DEFAULT", builder["ll"])
    }
}