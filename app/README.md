
# Get Started

Checkout the Project

`git clone https://marcosgribel@bitbucket.org/crack-interview-challanges/adyen-senior-android-assignment-marcos-gribel.git`


## OVERALL

The project leverages on several modern approaches to build Android applications with:

* Kotlin ❤️
* MVVM
* Clean Archictecture
* Unidirectional Data Flow
* Powered by Android JetPack
* Ready Easy code modularization
* Material Design guideline
* Unit Tests ❤❤️️

<br>

**Libraries Highlight**

JetPack Compose, Dagger/Hilt, Retrofit, Accompanist, Mockk


_Additionally, the project is neatly organized by packages following a basic clean architecture layer structure (data, domain, presentation) and it could easily be modularised if needed._

## NOTES

###  Location
I decided to use the `​​FusedLocationProviderClient` as the primary source to fetch  the user's last known location. It is limited to using only the network provider (`ACCESS_COARSE_LOCATION`) and for the use case of showing nearby places, I think it is accurate enough not justifying using GPS (`ACCESS_FINE_LOCATION`) and consuming more user battery resources.

As an improvement, if there were more time available, I would suggest/implement a more user-friendly solution, giving the user a possibility to fetch the nearby places by an address input by the user.


### Other

Although there is an open space to add more features I wanted to keep the scope of the project small and focus more on the areas of Architecture, Clean Code, and Testability.

In addition, I took the challenge as an opportunity to experiment with the latest version of a few libraries and you might find a few of them annotated with @Experimental, mainly the Google Accompanist to handle runtime permission, however, bear in mind I would be careful in an enterprise application of using experimental libraries without an upon agreement with the team. 


