package com.adyen.android.assignment

import com.adyen.android.assignment.money.Bill
import com.adyen.android.assignment.money.Change
import com.adyen.android.assignment.money.Coin
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test

class CashRegisterTest {

    @Test(expected = CashRegister.TransactionException::class)
    fun `when the amount paid is zero then throw an exception`() {
        //GIVEN
        val price = 80_00L
        val amountPaid = Change.none()

        //WHEN
        CashRegister(Change.max()).performTransaction(price, amountPaid)

        //THEN
        fail(CashRegister.MESSAGE_NON_SUFFICIENT_FOUNDS)
    }

    @Test(expected = CashRegister.TransactionException::class)
    fun `when the amount paid is lower than the price then throw an exception`() {
        //GIVEN
        val price = 80_00L
        val amountPaid = Change().add(Bill.FIFTY_EURO, 1)

        //WHEN
        CashRegister(Change.max()).performTransaction(price, amountPaid)

        //THEN
        fail(CashRegister.MESSAGE_NON_SUFFICIENT_FOUNDS)
    }

    @Test
    fun `when the amount paid matches exactly the price than return an empty change`() {
        //GIVEN
        val price = 80_00L
        val amountPaid = Change().apply {
            add(Bill.FIFTY_EURO, 1)
            add(Bill.TEN_EURO, 1)
            add(Bill.TWENTY_EURO, 1)
        }

        //WHEN
        val expected = Change.none()
        val actual = CashRegister(Change.max()).performTransaction(price, amountPaid)

        //THEN
        assertEquals(expected, actual)
    }

    @Test(expected = CashRegister.TransactionException::class)
    fun `when there is not enough change in the cash register then return an exception`() {
        //GIVEN
        val change = Change().apply {
            add(Bill.FIFTY_EURO, 1)
            add(Bill.TEN_EURO, 1)
        }
        val price = 30L
        val amountPaid = Change().add(Bill.FIFTY_EURO, 1)

        //WHEN
        CashRegister(change).performTransaction(price, amountPaid)

        //THEN
        fail(CashRegister.MESSAGE_NON_SUFFICIENT_FOUNDS)
    }

    @Test
    fun `when there is enough change in the cash register then return the correct change`() {
        //GIVEN
        val change = Change().apply{
            Bill.values().forEach { add(it, 2) }
            Coin.values().forEach { add(it, 2) }
        }

        val price = 35_00L
        val amountPaid = Change().apply {
            add(Bill.FIFTY_EURO, 1)
        }

        //WHEN
        val expected = Change().apply {
            add(Bill.TEN_EURO, 1)
            add(Bill.FIVE_EURO, 1)
        }
        val actual = CashRegister(change).performTransaction(price, amountPaid)

        //THEN
        assertEquals(expected, actual)
    }

    @Test
    fun `given a amount paid with bills and coins when there is enough change in the cash register then return the correct change`() {
        //GIVEN
        val change = Change().apply{
            Bill.values().forEach { add(it, 2) }
            Coin.values().forEach { add(it, 2) }
        }

        val price = 35_75L
        val amountPaid = Change().apply {
            add(Bill.FIFTY_EURO, 1)
        }

        //WHEN
        val expected = Change().apply {

            add(Bill.TEN_EURO, 1)
            add(Coin.TWO_EURO, 2)
            add(Coin.TWENTY_CENT, 1)
            add(Coin.FIVE_CENT, 1)

        }
        val actual = CashRegister(change).performTransaction(price, amountPaid)

        //THEN
        assertEquals(expected, actual)
    }

    @Test
    fun `when multiple transactions are performed then keep track of the money that is in the cash register`() {
        //GIVEN
        val monetaryElementCount = 3
        val bills = Bill.values()
        val coins = Coin.values()

        val change = Change().apply {
            bills.forEach {  add(it, monetaryElementCount)  }
            coins.forEach {  add(it, monetaryElementCount)  }
        }

        val givenPriceAndAmount = listOf(
            Pair(
                150_00L,
                Change().apply {
                    add(Bill.TWO_HUNDRED_EURO, 1)
                },
            ),
            Pair(
                75_00L,
                Change().apply {
                    add(Bill.ONE_HUNDRED_EURO, 1)
                },
            ),
            Pair(
                75L,
                Change().apply {
                    add(Coin.ONE_EURO, 1)
                },
            ),
        )

        //WHEN
        val expected = Change().apply {
            // init
            bills.forEach {  add(it, monetaryElementCount)  }
            coins.forEach {  add(it, monetaryElementCount)  }

            // Received from shoppers
            add(Bill.TWO_HUNDRED_EURO, 1)
            add(Bill.ONE_HUNDRED_EURO, 1)
            add(Coin.ONE_EURO, 1)

            // Returned as change to the shoppers
            remove(Bill.FIFTY_EURO, 1)
            remove(Bill.TWENTY_EURO, 1)
            remove(Bill.FIVE_EURO, 1)
            remove(Coin.TWENTY_CENT, 1)
            remove(Coin.FIVE_CENT, 1)
        }

        val cashRegister = CashRegister(change)
        givenPriceAndAmount.map {
            cashRegister.performTransaction(it.first, it.second)
        }

        //THEN
        assertEquals(expected, change)

    }

}
