package com.adyen.android.assignment

import com.adyen.android.assignment.money.Change
import kotlin.math.min

/**
 * The CashRegister class holds the logic for performing transactions.
 *
 * @param change The change that the CashRegister is holding.
 */
class CashRegister(private val change: Change) {

    companion object {
        const val MESSAGE_NON_SUFFICIENT_FOUNDS = "Non-sufficient founds."
    }


    /**
     * Performs a transaction for a product/products with a certain price and a given amount.
     *
     * @param price The price of the product(s).
     * @param amountPaid The amount paid by the shopper.
     *
     * @return The change for the transaction.
     *
     * @throws TransactionException If the transaction cannot be performed.
     */
    fun performTransaction(price: Long, amountPaid: Change): Change {
        return when {
            price > amountPaid.total -> throw TransactionException(MESSAGE_NON_SUFFICIENT_FOUNDS)
            else -> change(price, amountPaid)
        }
    }

    private fun change(
        price: Long,
        inChange: Change,
    ): Change {

        var amountBack = inChange.total - price

        if (amountBack == 0L) return Change.none()

        val outChange = Change()
        val elements = change.getElements()
        val numOfElements = elements.size

        /*
        The change will be removed/withdraw from the till beginning with bills and moving to coins
        (The ThreeMap is sorted in a descending order)
        */
        var i = 0
        while (numOfElements > i && amountBack != 0L) {

            val element = elements.elementAt(i)

            val numOfElementAvailable = amountBack / element.minorValue

            if (numOfElementAvailable >= 1) {
                val amountOfElementBack = min(
                    numOfElementAvailable.toInt(),
                    change.getCount(element)
                )

                outChange.add(element, amountOfElementBack)
                change.remove(element, amountOfElementBack)

                amountBack -= (element.minorValue * amountOfElementBack)
            }

            i++
        }

        if (amountBack != 0L) throw TransactionException(MESSAGE_NON_SUFFICIENT_FOUNDS)

        /* If the change succeed, add the income changed to the till */
        change.add(inChange)

        return outChange
    }

    class TransactionException(message: String, cause: Throwable? = null) :
        Exception(message, cause)
}
